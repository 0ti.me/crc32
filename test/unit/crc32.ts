import { d, expect } from '@0ti.me/ts-test-deps';
import { crc32 } from '../../src/index';

const me = __filename;

d(me, () => {
  it('should calculate the correct crc32 for a given buffer', () => {
    [
      { input: 'hello world', expected: 222957957 },
      {
        input:
          'some really long string full of gibberish for which we should compute a crc32',
        expected: 3712875809,
      },
    ].forEach(({ input, expected }) =>
      expect(crc32(Buffer.from(input, 'utf8'))).to.equal(expected),
    );
  });

  it('should use previous or something', () =>
    expect(crc32(Buffer.from('hello world'), 0x11ec11ec)).to.equal(2216331641));

  it('should use previous = 0 or something', () =>
    expect(crc32(Buffer.from('hello world'), 0)).to.equal(2570215318));
});
